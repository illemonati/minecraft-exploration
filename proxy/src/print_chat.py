"""
Prints out chat messages
"""

from twisted.internet import reactor
from quarry.types.uuid import UUID
from quarry.net.proxy import DownstreamFactory, Bridge, UpstreamFactory
from quarry.net.auth import Profile, ProfileCLI
import certifi
import getpass
import os

from shared import MyBridge, MyDownstreamFactory, MyDownstream

EMAIL = "tonymiaotong@gmail.com"
REMOTE_HOST = "mc.hypixel.net"
REMOTE_PORT = 25565
LOCAL_PORT = 15565


class PrintChatBridge(MyBridge):
    accessToken = os.environ["MINECRAFT_TOKEN"]


class PrintChatDownstreamFactory(MyDownstreamFactory):
    bridge_class = PrintChatBridge


def main():
    print(os.environ["MINECRAFT_TOKEN"])
    os.environ['SSL_CERT_FILE'] = certifi.where()

    factory = PrintChatDownstreamFactory()
    factory.connect_port = REMOTE_PORT
    factory.connect_host = REMOTE_HOST

    print(REMOTE_HOST)
    print(factory.connect_host)

    factory.listen("localhost", LOCAL_PORT)
    reactor.run()


if __name__ == "__main__":
    main()
